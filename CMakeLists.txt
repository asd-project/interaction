#--------------------------------------------------------
#   asd interaction
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

#--------------------------------------------------------

project(interaction VERSION 0.0.1)

#--------------------------------------------------------

include(bootstrap)
include(module)

#--------------------------------------------------------

module(STATIC)
    domain(uio)

    group(include Headers)
    files(
        keyboard_manipulator.h
        mouse_manipulator.h
        mouse_navigator.h
    )

    group(src Sources)
    files(
        keyboard_manipulator.cpp
        mouse_manipulator.cpp
    )
endmodule()

#--------------------------------------------------------
