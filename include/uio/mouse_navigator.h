//---------------------------------------------------------------------------

#pragma once

#ifndef INTERACTION_MOUSE_NAVIGATOR_H
#define INTERACTION_MOUSE_NAVIGATOR_H

//---------------------------------------------------------------------------

#include <uio/mouse.h>
#include <math/point.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        class mouse_navigator
        {
        public:
            mouse_navigator(uio::mouse_input & input) :
                _input(input),
                _move_observer(input.on_move, [this](const uio::mouse_move_event & e) {
                    if (_anchor) {
                        _panning = _anchor->panning + (e.mouse.position() - _anchor->mouse_pos) / _zoom;
                    }
                }),
                _wheel_observer(input.on_wheel, [this](const uio::mouse_wheel_event & e) {
                    _zoom_exp += e.delta.y * _zoom_speed;
                    _zoom = std::pow(10.0f, _zoom_exp);
                })
            {}

            const math::float_point & panning() const {
                return _panning;
            }

            float zoom() const {
                return _zoom;
            }

            float zoom_speed() const {
                return _zoom_speed;
            }

            void set_zoom_speed(float zoom_speed) {
                _zoom_speed = zoom_speed;
            }

            void enable() {
                if (_anchor) {
                    return;
                }

                _anchor = anchor{math::float_point(_input.position()), _panning};
            }

            void disable() {
                _anchor = {};
            }

        protected:
            struct anchor
            {
                math::float_point mouse_pos = {0.0f, 0.0f};
                math::float_point panning = {0.0f, 0.0f};
            };

            uio::mouse_input & _input;
            signal::observer<void(const uio::mouse_move_event &)> _move_observer;
            signal::observer<void(const uio::mouse_wheel_event &)> _wheel_observer;

            std::optional<anchor> _anchor;
            math::float_point _panning = {0.0f, 0.0f};
            float _zoom = 1.0f;
            float _zoom_exp = 0.0f;
            float _zoom_speed = 0.1f;
        };
    }
}

//---------------------------------------------------------------------------
#endif
