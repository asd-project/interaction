//---------------------------------------------------------------------------

#pragma once

#ifndef INTERACTION_KEYBOARD_MANIPULATOR_H
#define INTERACTION_KEYBOARD_MANIPULATOR_H

//---------------------------------------------------------------------------

#include <uio/keyboard.h>
#include <math/point.h>
#include <container/enum_index_array.h>
#include <optional>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        enum class direction
        {
            up,
            left,
            down,
            right
        };

        using movement_layout = asd::enum_index_array<uio::direction, uio::scancode>;
        
        enum class movement_layout_preset
        {
            arrows,
            wasd
        };

        class keyboard_manipulator
        {
        public:
            export_api(interaction)
            keyboard_manipulator(const uio::keyboard_input & keyboard);

            export_api(interaction)
            math::point<float> delta(float amplitude, uio::movement_layout_preset layout) const;
            
            export_api(interaction)
            math::point<float> delta(float amplitude, const uio::movement_layout & layout) const;

        private:
            const uio::keyboard_input & _keyboard;
        };
    }
}

//---------------------------------------------------------------------------
#endif
