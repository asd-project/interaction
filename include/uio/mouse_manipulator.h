//---------------------------------------------------------------------------

#pragma once

#ifndef MOUSE_MANIPULATOR_H
#define MOUSE_MANIPULATOR_H

//---------------------------------------------------------------------------

#include <uio/mouse.h>
#include <signal/observer.h>
#include <math/point.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        class mouse_manipulator
        {
        public:
            export_api(interaction)
            mouse_manipulator(uio::mouse_input & input, uio::mouse_output & output);
            export_api(interaction)
            ~mouse_manipulator();

            export_api(interaction)
            math::float_point fetch_delta();

            export_api(interaction)
            void enable();
            export_api(interaction)
            void disable();

            export_api(interaction)
            void toggle();

        protected:
            export_api(interaction)
            void add_delta(const math::float_point & delta);

            bool _enabled = false;

            uio::mouse_output & _output;
            signal::observer<void(const uio::mouse_move_event &)> _observer;

            math::float_point _delta = {0.0f, 0.0f};
        };
    }
}

//---------------------------------------------------------------------------
#endif
