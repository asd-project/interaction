//---------------------------------------------------------------------------

#include <uio/mouse_manipulator.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        static const math::int_point center {5, 5};

        mouse_manipulator::mouse_manipulator(uio::mouse_input & input, uio::mouse_output & output) :
            _output(output),
            _observer(input.on_move, [this](const uio::mouse_move_event & e) {
                if (_enabled) {
                    add_delta(e.delta);
                }
            })
        {}

        mouse_manipulator::~mouse_manipulator() {
            disable();
        }

        math::float_point mouse_manipulator::fetch_delta() {
            if (!_enabled) {
                return {0.0f, 0.0f};
            }

            // remove peaks
            _delta.x = math::clamp(_delta.x, -center.x * 0.8f, center.x * 0.8f);
            _delta.y = math::clamp(_delta.y, -center.y * 0.8f, center.y * 0.8f);

            // increase smoothness
            _delta.x = math::pow(_delta.x / center.x, 3.0f) * center.x;
            _delta.y = math::pow(_delta.y / center.y, 3.0f) * center.y;

            return std::exchange(_delta, {0.0f, 0.0f});
        }

        void mouse_manipulator::enable() {
            if (!_enabled) {
                toggle();
            }
        }

        void mouse_manipulator::disable() {
            if (_enabled) {
                toggle();
            }
        }

        void mouse_manipulator::toggle() {
            _enabled = !_enabled;
            _output.set_relative_mode(_enabled);
        }

        void mouse_manipulator::add_delta(const math::float_point & delta) {
            _delta += delta;
        }
    }
}

//---------------------------------------------------------------------------
